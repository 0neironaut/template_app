%%% coding: utf-8
%%% @author {{author_name}} <{{author_email}}>
%%% @date {{date}}
%% @doc

-module({{name}}_supv).
-author('{{author_name}} <{{author_email}}>').

-behaviour(supervisor).

-export([init/1]).
-export([start_link/1]).
-export([start_child/1, restart_child/1, terminate_child/1, delete_child/1, get_childspec/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/{{name}}.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(StartArgs) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, StartArgs).

start_child(ChildSpec) ->
    supervisor:start_child(?MODULE, ChildSpec).

restart_child(Id) ->
    supervisor:restart_child(?MODULE, Id).

terminate_child(Id) ->
    supervisor:terminate_child(?MODULE, Id).

delete_child(Id) ->
    supervisor:delete_child(?MODULE, Id).

get_childspec(Id) ->
    supervisor:get_childspec(?MODULE, Id).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(StartArgs) ->
    Spec = [#{id => ?SRV,
              start => {?SRV, start_link, [[]]},
              restart => permanent,
              shutdown => 1000,
              type => worker,
              modules => [?SRV]}],
    Res = supervisor:check_childspecs(Spec),
    ?OUT("~p: supervisor check childspecs result (~120p)", [?ServiceName,Res]),
    ?OUT("~p: supervisor inited (~120p)", [?ServiceName,StartArgs]),
    SupFlags = #{strategy => one_for_one,
                 intensity => 10,
                 period => 2},
    {ok, {SupFlags, Spec}}.

%% ====================================================================
%% Internal functions
%% ====================================================================
